const localPlayer = mp.players.local;
let isFactionControlOpen = false;
mp.keys.bind(0x74, true, function () {
  isFactionControlOpen = !isFactionControlOpen;
  mp.gui.call("cef:user.openFaction", isFactionControlOpen);
});

mp.events.add("client:faction.getFactionList", () => {
  mp.events.callRemote("server:faction.getFactionList");
});

mp.events.add("client:faction.setFactionList", (factionList) => {
  mp.gui.call("cef:faction.setFactionList", factionList);
});

mp.events.add("client:faction.setMembershipApplication", (_id, name) => {
  mp.events.callRemote("server:faction.setMembershipApplication", _id, name);
});

mp.events.add("client:faction.updateFactionState", (charFaction) => {
  mp.gui.call("cef:faction.updateFactionState", charFaction);
});

mp.events.add("client:faction.leaveCurrentFaction", (name, _id) => {
  mp.events.callRemote("server:faction.leaveCurrentFaction", name, _id);
});
