let openWeaponList = true;
mp.events.add("client:weapons.giveWeaponToPlayer", (weaponName, clips) => {
  mp.events.callRemote("server:weapons.giveWeaponToPlayer", weaponName, clips);
  mp.events.callRemote("devVeh", `${weaponName} -- ${clips}`);
});
mp.keys.bind(0x73, true, function () {
  mp.gui.call("cef:user.showWeaponList", openWeaponList);

  openWeaponList = !openWeaponList;
});
