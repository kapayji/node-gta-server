$clientPrefix = "client:accounts.";
$serverPrefix = "server:accounts.";

const localPlayer = mp.players.local;

mp.events.add($clientPrefix + "loginAttempt", (email, password) => {
  mp.events.callRemote($serverPrefix + "onLoginAttempt", email, password);
});
mp.events.add($clientPrefix + "registrationAttempt", (email, password) => {
  mp.events.callRemote(
    $serverPrefix + "onRegistrationAttempt",
    email,
    password
  );
});

mp.events.add("browserExecute", () => {
  mp.gui.execute("window.location = 'http://localhost:8080/auth'");
  mp.gui.cursor.show(true, true);
});
mp.events.add($clientPrefix + "successNotify", (message) => {
  mp.gui.call("cef:loginform.successNotify", message);
});
mp.events.add($clientPrefix + "unsuccessNotify", (message) => {
  mp.gui.call("cef:loginform.unsuccessNotify", message);
});
mp.events.add($clientPrefix + "loginSuccess", (user, characters) => {
  mp.gui.call("cef:accounts.loginSuccess", user, characters);
});

mp.events.add($clientPrefix + "updateInDB", (updatingValue, payload) => {
  mp.events.callRemote($serverPrefix + "updateInDB", updatingValue, payload);
});

//cursor
let cursor = false;
mp.keys.bind(0x71, true, function () {
  cursor = !cursor;
  mp.gui.cursor.show(cursor, cursor);
});
// mp.keys.bind(0x71, false, function () {
//   mp.gui.cursor.show(false, false);
// });
mp.events.add($clientPrefix + "getPos", () => {
  mp.gui.call("cef:user.getPos", localPlayer.position);
});

///Char create
mp.events.add("client:accounts.charCreateAttempt", (nickname, sex, id) => {
  mp.events.callRemote("server:accounts.charCreateAttempt", nickname, sex, id);
});
mp.events.add("client:accounts.successCharacterCreate", (character) => {
  mp.gui.call("cef:character.createSuccess", character);
});
mp.events.add("client:accounts.charLoadAttempt", (id) => {
  mp.events.callRemote("server:accounts.charLoadAttempt", id);
});
mp.events.add("client:accounts.successLoadingCharacter", (character) => {
  mp.events.callRemote("server:accounts.playerSpawn");
  mp.gui.call("cef:character.successLoginCharacter", character);
  mp.gui.chat.activate(true);
});
mp.events.add("client:accounts.playerDeath", () => {
  mp.gui.call("cef:user.playerDeath");
});
mp.events.add("client:accounts.playerHospitalRevive", () => {
  mp.events.callRemote("server:accounts.playerHospitalRevive");
});
mp.events.add("playerCreateWaypoint", (position) => {
  let newPositionZ = mp.game.gameplay.getGroundZFor3dCoord(
    position.x,
    position.y,
    25,
    0,
    false
  );
  mp.players.local.position = new mp.Vector3(
    position.x,
    position.y,
    newPositionZ
  );
});
