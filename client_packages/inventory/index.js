const localPlayer = mp.players.local;
let isInventoryOpen = false;
mp.keys.bind(0x49, true, function () {
  isInventoryOpen = !isInventoryOpen;
  mp.gui.call("cef:user.openInventory", isInventoryOpen);
});
