let openVehList = true;
mp.events.add("client:vehicles.createVehicle", (model, position) => {
  mp.events.callRemote("server:vehicles.createVehicle", model, position);
});
mp.keys.bind(0x72, true, function () {
  mp.gui.call("cef:user.vehiclelist", openVehList);
  openVehList = !openVehList;
});

mp.events.add("client:vehicles.playerExitVehicle", () => {
  mp.gui.call("cef:user.playerLeaveVehicle");
});
mp.events.add("client:vehicles.playerEnterVehicle", (seat) => {
  if (seat === 0) {
    mp.gui.call("cef:user.playerSitInVehicle");
  }
});
// let maxFuel = mp.players.local.vehicle.fuel;

mp.events.add("render", () => {
  if (mp.players.local.vehicle) {
    if (mp.players.local.vehicle.getPedInSeat(-1) === mp.players.local.handle) {
      mp.gui.call(
        "cef:user.vehicleStatus",
        mp.game.vehicle.getVehicleModelMaxSpeed(
          mp.players.local.vehicle.model
        ) * 3.6,
        mp.players.local.vehicle.getSpeed() * 3.6,
        100,
        mp.players.local.vehicle.rpm
      );
    } else {
      mp.gui.call("cef:user.playerLeaveVehicle");
    }
  }
});
mp.events.add("client:sendMesToServer", (mes) => {
  mp.events.callRemote("devVeh", mes);
});

mp.keys.bind(0x74, true, function () {
  mp.events.callRemote("dist");
  let icon = loadTextureDictionary("CHAR_ACTING_UP");
  mp.gui.call(
    "cef:user.infoNotify",
    "Называние",
    "От кого",
    "Содержание уведомления",
    icon
  );
});
