module.exports = {
  async up(db, client) {
    return db.collection("characters").updateMany(
      {},
      {
        $set: {
          faction: [
            {
              factionName: String,
              factionPosition: String,
              factionRights: Number,
            },
          ],
        },
      }
    );
  },
  async down(db, client) {
    // TODO write the statements to rollback your migration (if possible)
    // Example:
    // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
  },
};
