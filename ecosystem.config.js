module.exports = {
  apps : [{
    script: 'ragemp-server.exe',
    watch: ['packages', 'client_packages'],
    ignore_watch: ['node_modules', 'vue-cef-client', 'client_packages/.listcache'],
    watch_delay: 5000,
  }],
};
