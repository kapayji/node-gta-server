import { createRouter, createWebHistory } from "vue-router";
const routerHistory = createWebHistory();
import HomePage from "@/pages/HomePage";
import AuthPage from "@/pages/AuthPage";
import UserPage from "@/pages/UserPage";
import TestDev from "@/pages/TestDev";
import CharactersPage from "@/pages/CharactersPage";
const router = createRouter({
  history: routerHistory,
  routes: [
    {
      path: "/",
      name: "home",
      component: HomePage,
      props: true,
    },
    {
      path: "/auth",
      name: "auth",
      component: AuthPage,
    },
    {
      path: "/character",
      name: "character",
      component: CharactersPage,
    },
    {
      path: "/user/:id",
      name: "user",
      component: UserPage,
    },
    {
      path: "/test",
      name: "test",
      component: TestDev,
    },
  ],
});
export default router;
