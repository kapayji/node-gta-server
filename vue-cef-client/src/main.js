import { createApp } from "vue";
import store from "./store/index";
import router from "./routes";
import App from "./App.vue";
import "./notifystyle.css";
import "./utils/validation/validationRules.js";
import "./assets/css/bootstrap.css";
import "./style.css";

const app = createApp(App);
app
  .use(router)
  .use(store)
  .mount("#app");
