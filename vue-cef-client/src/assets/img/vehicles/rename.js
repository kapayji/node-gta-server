const fs = require("fs")

fs.readdir("./", (err, files) => {
	
  for (const file of files) {
	  let arr = []
    if (/\-/g.test(file)) {
		arr = file.split('-');
      fs.rename(file, (`${arr[1]}`).toLowerCase(), (err) => {
        console.log('Renaming', file, "to", `${arr[1]}`)
        if (err) throw err
      })
    }
  }
})