import { createStore } from "vuex";

const store = createStore({
  state: {
    user: [],
    characters: [],
    currentCharacter: [],
  },
  getters: {
    getPlayer(state) {
      return state.user;
    },
    getMoney(state) {
      return state.currentCharacter.money;
    },
    getCharacters(state) {
      return state.characters;
    },
    getCurrentCharacter(state) {
      return state.currentCharacter;
    },
  },

  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
    setCharacters(state, characters) {
      state.characters = characters;
    },
    addNewCharacter(state, character) {
      state.characters.push(character);
    },
    setCurrentCharacter(state, currentCharacter) {
      state.currentCharacter = currentCharacter;
    },
    setMoney(state, payload) {
      state.currentCharacter.money = payload;
    },
    setCharacterFactionState(state, charFaction) {
      state.currentCharacter.faction = charFaction;
    },
  },
  actions: {
    setUser({ commit }, payload) {
      commit("setUser", payload);
    },
    setMoney({ commit }, payload) {
      commit("setMoney", payload);
    },
    setCharacters({ commit }, characters) {
      commit("setCharacters", characters);
    },
    addNewCharacter({ commit }, character) {
      commit("addNewCharacter", character);
    },
    setCurrentCharacter({ commit }, currentCharacter) {
      commit("setCurrentCharacter", currentCharacter);
    },
    setCharacterFactionState({ commit }, charFaction) {
      commit("setCharacterFactionState", charFaction);
    },
  },
});
export default store;
