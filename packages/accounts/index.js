$clientPrefix = "client:accounts.";
$serverPrefix = "server:accounts.";

const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const { Account, Character } = require("../mongo/index");

const MONGODB_URL =
  "mongodb+srv://user:user@cluster0.iic1p.mongodb.net/rage?retryWrites=true&w=majority";

mp.events.add("packagesLoaded", async () => {
  try {
    await mongoose
      .connect(MONGODB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
      })
      .then(console.log("Success connection"));
  } catch (e) {
    console.log(e.message);
    process.exit(1);
  }
});

mp.events.add("playerReady", (player) => {
  player.call("browserExecute");
});
mp.events.add(
  $serverPrefix + "onLoginAttempt",
  async (player, email, password) => {
    try {
      const user = await Account.findOne({ email });
      if (!user) {
        return player.call($clientPrefix + "unsuccessNotify", [
          "Player not found",
        ]);
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
        return player.call($clientPrefix + "unsuccessNotify", [
          "Incorrect password, try again",
        ]);
      }
      const characters = await Character.find({ owner: user._id });
      console.log(characters);
      player.call($clientPrefix + "successNotify", ["You are logged in"]);
      player.call($clientPrefix + "loginSuccess", [user, characters]);
    } catch (e) {
      console.log(e);
    }
  }
);

mp.events.add(
  "server:accounts.charCreateAttempt",
  async (player, nickname, sex, id) => {
    console.log(`From client - ${id}`);
    try {
      const nickNameExist = await Character.findOne({ nickname });
      if (nickNameExist) {
        return player.call("client:accounts.unsuccessNotify", [
          "Nickname alredy exist",
        ]);
      }
      const character = await new Character({ nickname, sex, owner: id });

      player.call("client:accounts.successNotify", ["Character created"]);
      // await character.save();
      character.faction.push({
        factionName: "None",
        factionPosition: "",
        factionRights: 0,
      });
      await character.save();
      // console.log(character.faction.factionName);
      player.call("client:accounts.successCharacterCreate", [character]);
    } catch (e) {
      console.log(e);
    }
  }
);

mp.events.add(
  $serverPrefix + "onRegistrationAttempt",
  async (player, email, password) => {
    try {
      const candidate = await Account.findOne({ email });
      if (candidate) {
        return player.call($clientPrefix + "unsuccessNotify", [
          "This Email alredy exist",
        ]);
      }
      const hashedPassword = await bcrypt.hash(password, 12);
      const user = new Account({
        email,
        password: hashedPassword,
      });
      await user.save();
      return player.call($clientPrefix + "successNotify", ["Player created"]);
    } catch (e) {
      console.log(e);
    }
  }
);

mp.events.add("server:accounts.charLoadAttempt", async (player, _id) => {
  try {
    const charDB = await Character.find({ _id });
    if (!charDB) {
      return player.call("client:accounts.unsuccessNotify", [
        "Character no find in DB",
      ]);
    }
    player.name = charDB[0].nickname;
    player.call("client:accounts.successLoadingCharacter", charDB);
    player.call("client:accounts.successNotify", [
      "Character load successeful",
    ]);
    console.log(charDB);
  } catch (e) {
    console.log(e);
  }
});

mp.events.add(
  $serverPrefix + "updateInDB",
  async (player, updatingValue, payload) => {
    try {
      const updatedPlayer = await Character.findOneAndUpdate(
        { _id: payload },
        { money: updatingValue }
      );
    } catch (e) {
      console.log(e);
    }
  }
);

mp.events.add("devPosition", (player, position) => {
  console.log(position);
});
mp.events.add("devVeh", (player, mes) => {
  console.log(mes);
});

///

mp.events.add("playerDeath", (player) => {
  player.call("client:accounts.playerDeath");
});
mp.events.add("server:accounts.playerHospitalRevive", (player) => {
  player.health = 100;
  const pos = player.position;
  player.spawn(
    new mp.Vector3(461.1863708496094, -611.451171875, 29.330766677856445)
  );
});

mp.events.add("server:accounts.playerSpawn", (player) => {
  player.spawn(
    new mp.Vector3(461.1863708496094, -611.451171875, 29.330766677856445)
  );
});
