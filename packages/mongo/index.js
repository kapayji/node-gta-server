const mongoose = require("mongoose");

const schemaCharacters = new mongoose.Schema({
  nickname: { type: String, require: true, unique: true },
  sex: { type: String, required: true },
  money: { type: Number, required: true, default: 0 },
  inventoryItems: { type: Array, require: true, default: [] },
  owner: { type: mongoose.ObjectId, ref: "Account" },
  faction: [
    {
      factionName: String,
      factionPosition: String,
      factionRights: Number,
    },
  ],
});

const Character = mongoose.model("Character", schemaCharacters);

const schemaRegistration = new mongoose.Schema({
  email: { type: String, unique: true, required: true },
  password: { type: String, require: true },
  character: [{ type: mongoose.ObjectId, ref: "Character" }],
});
const Account = mongoose.model("Account", schemaRegistration);

const schemaFactions = new mongoose.Schema({
  factionName: { type: String, required: true },
  members: [{ type: mongoose.ObjectId, ref: "Character" }],
  deputy: [{ type: mongoose.ObjectId, ref: "Character" }],
  leader: [{ type: mongoose.ObjectId, ref: "Character" }],
});

const Factions = mongoose.model("Factions", schemaFactions);

module.exports = { Account, Character, Factions };
