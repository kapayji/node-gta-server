mp.events.add("playerEnterVehicle", (player, vehicle, seat) => {
  // player.outputChatBox(`${player.name} got into the car. Seat: ${seat}`);
  console.log(`${player.seat} sit in car`);
  console.log(`${vehicle.id}`);
  console.log(`${seat}`);
  console.log(`${vehicle.model}`);

  player.call("client:vehicles.playerEnterVehicle", [seat]);
});
mp.events.add("playerExitVehicle", (player, vehicle) => {
  player.call("client:vehicles.playerExitVehicle");
  console.log(`exit from vehicle id ${vehicle.id}`);
});

mp.events.add("server:vehicles.devVeh", (_, mes) => {
  console.log(mes);
});

mp.events.add("server:vehicles.createVehicle", (player, model, position) => {
  let pos = JSON.parse(position);
  createVehicle(model, pos);
  // console.log(pos);
});

function createVehicle(model, pos) {
  let vehicle = mp.vehicles.new(
    mp.joaat(model),
    new mp.Vector3(pos.x, pos.y, pos.z),
    {
      numberPlate: "KapayJI",
    }
  );
  mp.labels.new("Label", new mp.Vector3(pos.x, pos.y, pos.z), {
    los: false,
    font: 1,
    drawDistance: 100,
  });
  return vehicle;
}

mp.events.add("dist", (player) => {
  mp.vehicles.forEach((v) => {
    console.log(`Distance: ${player.dist(v.position)}`);
  });
});
