mp.events.add(
  "server:weapons.giveWeaponToPlayer",
  (player, weaponName, clips) => {
    player.giveWeapon(mp.joaat(weaponName), clips);
    console.log(`${weaponName} -- ${clips}`);
  }
);
