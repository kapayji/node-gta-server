const mongoose = require("mongoose");

const { Factions, Characters, Character } = require("../mongo/index");
mp.events.add("server:faction.getFactionList", async (player) => {
  try {
    const factionList = await Factions.find();
    console.log(factionList);
    return player.call("client:faction.setFactionList", [factionList]);
  } catch (e) {
    console.log(e);
  }
});
mp.events.add(
  "server:faction.setMembershipApplication",
  async (player, _id, factionName) => {
    console.log(_id);
    const isExist = await Factions.findOne({
      members: { _id },
      deputy: { _id },
      leader: { _id },
    });
    if (isExist) {
      console.log(isExist);
      // return player.call($clientPrefix + "unsuccessNotify", ["Вы уже состоите во фракции"]);
    } else {
      const newMember = await Factions.findOne({ factionName });
      newMember.members.push(_id);
      await newMember.save();
      const characterApplyToFaction = await Character.findById({ _id });
      characterApplyToFaction.faction[0].factionName = factionName;
      characterApplyToFaction.faction[0].factionPosition = "Newbie";
      characterApplyToFaction.save();
      player.call("client:faction.updateFactionState", [
        characterApplyToFaction.faction[0],
      ]);
    }
  }
);
mp.events.add("server:faction.kickMember", async (player, _id) => {
  const memberForKick = await Factions.findOne({
    members: { _id },
  });
  const updateFaction = await Factions.members.pull({ members: memberForKick });
  updateFaction.save();
});

mp.events.add(
  "server:faction.leaveCurrentFaction",
  async (player, name, _id) => {
    const updateFactionMembers = await Factions.findOne({
      factionName: name,
    });
    await updateFactionMembers.members.pull(_id);
    updateFactionMembers.save();
    const characterLeaveFaction = await Character.findById({ _id });
    characterLeaveFaction.faction[0].factionName = "None";
    characterLeaveFaction.faction[0].factionPosition = "";
    characterLeaveFaction.save();
    player.call("client:faction.updateFactionState", [
      characterLeaveFaction.faction[0],
    ]);
  }
);
